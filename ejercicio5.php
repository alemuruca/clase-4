<!DOCTYPE html>
<html>
	<head>
		<title>Ejercicio 5</title>
		<style>
			#tabla{
				border:1px solid silver;
				border-collapse:collapse;
				padding:10px;
				text-align:center;
			}
			
			#tabla td{
				width:150px;
			}
			
			.claro{
				background:white;
			}
			
			.oscuro{
				background:rgba(225,225,225,1);
			}
		</style>
	</head>
	<body>
		<?php
			$nro = 9;
			echo "<h3>Tabla de multiplicar $nro </h3>";
			
			echo "<table id='tabla'>";
			for($i=1;$i<=9;$i++)
			{
				if($i%2 != 0)
					$clase="oscuro";
				else
					$clase="claro";
					
				echo "<tr class='$clase'>
					<td>$nro x $i = " . ($nro * $i) . "</td>
				</tr>";
			}
			echo "</table>";
		?>
	</body>
</html>